(function ($) {
        const DefaultOption = '<option selected="selected" value = "">Всё</option>';
        const ArrowUp = '<i class="material-icons text-muted align-bottom">keyboard_arrow_up</i>';
        const ArrowDown = '<i class="material-icons text-muted align-bottom">keyboard_arrow_down</i>';
        const ArrowForward = "<i class='material-icons text-muted'>arrow_forward_ios</i></div></td></tr>";

        let corpsProm, pupilsProms, dictProm, data, selectManager, templates, navbarHandler,
                html, sortingDirection, elementdata, element, modifiableElements,  setterName, mainPupilsTable, searchPupilsTable;

        templates = {
                corpsesTableTemplate: function (element) {
                        return  "<tr class = 'border-bottom' data-type ='corps' id =" +element.alias + 
                                "><td class='btn row border-0 px-4 d-flex justify-content-between align-items-center'>"+
                                "<div><p class = 'd-inline-block text-truncate'>"+
                                element.name + "</p>" + placesToHtml(element.places) +               
                                "</div><div class = 'd-flex align-items-center'>"+ ArrowForward

                                function placeTemplate(place){
                                        return "<p class='text-muted text-left'>" + place.code + "("+ place.count+ "ч.)" + "</p>"
                                }
                        
                                function placesToHtml(places){
                                        let _html = "";
                                        places.forEach(p => {_html += placeTemplate(p)})
                                        return _html;
                                }
                },
                pupilsTableTemplate: function (element) {
                        return "<tr><td class = 'align-middle'>"+ element.firstName + " " + element.lastName + " " + element.parentName +" </td><td><div>" + 
                        data.dictionary.audiences[element.audience] + isBel(element.needBel) +"</div><div>" +data.dictionary.places[element.place].code +
                        "</div></td><td></td></tr>";
                },
                placeSelectTemplate: function (element) {
                        return "<option value=" + element._id + ">" + element.code + "-" + element.count +" ч." + "</option>"
                },
                audienceSelectTemplate: function(element){
                        return "<option value=" + element._id + ">" + element.name + isBel(element.bel) + "-" + element.count +" ч." + "</option>"
                }
        }
        
        data = {
                corpses: [],
                pupils: [],
                dictionary: {},
                currentPupils: [],
                getCorps: function () {
                        return this.corpses.find(c => c.alias == selectManager.corps);
                },
                getPupils: function (selectMode) {
                        return this.pupils.filter(p => { return p[selectMode.key] == selectMode.value });
                }
        }

        navbarHandler = {
                menuClick: function(){
                       showSideBar(true)      
                },
                searchClick: function(){
                        modifiteElements();
                },
                backClick: function(){
                        modifiteElements();
                },
                findClick: function(){
                        let val = $("#input").val();
                        selectManager.setInput(val)
                }
        }

        selectManager = {
                corps: "",
                place: "",
                audience: "",
                input: "",
                setCorps: function (value) {
                        this.corps = value;
                        $('header a').html(data.getCorps().name)
                        initPlacesSelect();
                },

                setPlace: function (value = "") {
                        this.place = value;
                        if (value != "") {
                                initAudienceSelect(data.getCorps().places.filter(p => { return p._id == value }));
                        }
                        else initAudienceSelect(data.getCorps().places);
                },
                setAudience: function (value = "") {
                        this.audience = value;
                        data.currentPupils = data.getPupils(this.getSelectionMode());
                        fillTablePupils(mainPupilsTable, data.currentPupils);
                },
                setInput: function (value = "") {
                        if (value != "") {
                                let pupils = data.pupils.filter(p => p.firstName.includes(value));
                                fillTablePupils(searchPupilsTable ,pupils);
                        }
                        else this.resetInput();
                },
                resetInput: function () {
                        $("#searchPupilsTable tbody > tr").remove();
                },
                getSelectionMode: function () {
                        if (this.audience != "") return { key: "audience", value: this.audience };
                        if (this.place != "") return { key: "place", value: this.place };
                        if (this.corps != "") return { key: "corps", value: this.corps };
                }
        }

        sortingDirection = -1;
        searchPupilsTable = "searchPupilsTable";
        mainPupilsTable = "mainPupilsTable";

        corpsProm = requestGenerator('https://lyceumexams.herokuapp.com/api/corpses', "corpses");
        pupilsProms = requestGenerator('https://lyceumexams.herokuapp.com/api/pupils', "pupils");
        dictProm = requestGenerator('https://lyceumexams.herokuapp.com/api/dictionary', "dictionary");

        Promise.all([corpsProm, pupilsProms, dictProm]).then(initCorpsesTable); 

        $(document).on("click", ".navbar-toggler", navbarEvenHandler);
        $(document).on("click", "#sideNav tbody tr", corpsSelected);
        $(document).on("change", "select", selectChange);
        $(document).on("keyup", "input", selectChange);
        $(document).on("search", "input", () => selectManager.resetInput());
        $(document).on("click", ".sortable", sortTable);
        $(document).on("click", '.back-to-top', backToTop)
        $(window).scroll(showBackToTopButton)

        function navbarEvenHandler(e) {
                element = e.target.closest('.navbar-toggler');
                elementdata = element.getAttribute('data-type');
                setterName = elementdata + "Click";
                navbarHandler[setterName.toString()]();
        }

        function corpsSelected(e){
                showSideBar(false);
                element = e.target.closest('tr');
                selectManager.setCorps(element.id);
                
        }

        function selectChange(e) {
                let value, setterName;
                element = e.target;
                value = element.tagName == "SELECT" ? element.options[element.selectedIndex].value : element.value;
                elementdata = element.getAttribute('data-type').charAt(0).toUpperCase() + element.getAttribute('data-type').slice(1);
                setterName = 'set' + elementdata;
                selectManager[setterName.toString()](value);
        }

        function initPlacesSelect() {
                if(data.getCorps().places.length >1) html = DefaultOption;
                else html = "";
                data.getCorps().places.forEach(c => { html += templates.placeSelectTemplate(c) })
                $("#places").html(html).trigger('change');
        }

        function initAudienceSelect(places) {
                html = DefaultOption;
                for (p in places) {
                        places[p].audience.forEach(a => { html += templates.audienceSelectTemplate(a) });
                }
                $("#audiences").html(html).trigger('change');
        }

        function initCorpsesTable(){
                $('#corpsesTable tbody tr').remove();
                data.corpses.forEach(c => {html += templates.corpsesTableTemplate(c)});
                $("#corpsesTable tbody").html(html);

        }

        function showSideBar(flag){
               if(flag){
                $("#cap").removeClass('d-none');
                $("#sideNav").animate({'marginLeft': '+=300px'}, 250);  
               } 
               else{
                $("#cap").addClass('d-none');
                $("#sideNav").animate({'marginLeft': '-=300px'}, 250);
               }
        }

        function fillTablePupils(tableid,pupils) {
                html = "";
                $("#"+tableid+" tbody > tr").remove();
                $.each(pupils, (index) => { html += templates.pupilsTableTemplate(pupils[index], index) });
                $("#"+tableid+ " tbody").html(html);
                $('th>i').remove();
        }

        function sortTable(e) {
                let rows, dictionaryItem;
                elementdata = e.target.getAttribute('data-type');
                rows = data.currentPupils;
                sortingDirection *= -1;
                if (elementdata == 'firstName') {
                        rows.sort(sortTableWithoutDictionaryHelp);
                }
                else {
                        dictionaryItem = data.dictionary.audiences
                        rows.sort(sortTableWithDictionaryHelp);
                }
                fillTablePupils(mainPupilsTable, rows);

                sortingDirection > 0 ? $(e.target).append(ArrowUp) : $(e.target).append(ArrowDown);

                function sortTableWithoutDictionaryHelp(a, b) {
                        if (a[elementdata] < b[elementdata]) {
                                return -1 * sortingDirection;
                        }
                        if (a[elementdata] > b[elementdata]) {
                                return 1 * sortingDirection;
                        }
                        return 0;
                }

                function sortTableWithDictionaryHelp(a, b) {
                        if (dictionaryItem[a[elementdata]] < dictionaryItem[b[elementdata]]) {
                                return -1 * sortingDirection;
                        }
                        if (dictionaryItem[a[elementdata]] > dictionaryItem[b[elementdata]]) {
                                return 1 * sortingDirection;
                        }
                        return 0;
                }
        }

        function requestGenerator(url, objectName) {
                return $.get(url).then(requestSuccessful);
                function requestSuccessful(result) {
                        data[objectName] = result;
                }
        }

        function isBel(bel) {
                return bel ? "(бел)" : "";
        }

        function modifiteElements() {
                $(".hiddable").each(hideOrShowElement)                
        }

        function hideOrShowElement(index, value){
                if($(value).hasClass('d-none')){
                        $(value).removeClass('d-none');
                }
                else{
                        $(value).addClass('d-none');
                } 
        }

        function showBackToTopButton(e){
                if ($(window).scrollTop() > 100) {
                        $('#back-to-top').removeClass('d-none');
                    } else {
                        $('#back-to-top').addClass('d-none');
                    }
        }

        function backToTop(e){
                $('#back-to-top').addClass('d-none');
                $('body,html').animate({scrollTop: 0}, 500);   
        }

})(jQuery)