(function ($) {
        const DefaultOption = '<option selected="selected" value = "">Всё</option>';
        const EmptyOption = '<option id ="emptyOption"></option>';
        const ArrowUp = '<i class="fas fa-long-arrow-alt-up">';
        const ArrowDown = '<i class="fas fa-long-arrow-alt-down"></i>';

        let corpsProm, pupilsProms, dictProm, data, selectManager, templates,
                html, sortingDirection, number, elementdata, dragElement, dropElement, element;

        data = {
                corpses: [],
                pupils: [],
                dictionary: {},
                currentPupils: [],
                getCorps: function () {
                        return this.corpses.find(c => c.alias == selectManager.corps);
                },
                getPupils: function (selectMode) {
                        return this.pupils.filter(p => { return p[selectMode.key] == selectMode.value });
                },
                getPupilById: function (id) {
                        return this.pupils.find(p => p._id == id);
                },
                getAudienceByIdInCurrentCorps: function (id) {
                        let audience;
                        this.getCorps().places.forEach(p => audienceIsExist(p));
                        return audience;
                        function audienceIsExist(p) {
                                if (getAudienceByIdInPlace(p) != null) {
                                        audience = getAudienceByIdInPlace(p)
                                }
                        }
                        function getAudienceByIdInPlace(p) {
                                return p.audience.find(a => a._id == id);
                        }
                },
        }

        templates = {
                audiencesTableTemplate: function (element) {
                        return "<tr Class = 'droppable' id =" + element._id +
                                "></td><td>" + element.name +
                                "</td><td>" + element.count +
                                "</td><td>" + element.max +
                                "</td><td>" + "<div Class =" + isBel(element.bel) + "/>" + "</td></tr>"
                },
                pupilsTableTemplate: function (element, index) {
                        number = Number(index) + Number(1);
                        return "<tr Class = 'draggable' id =" + element._id +
                                "><td>" + number + "</td><td>"
                                + element.firstName + " " + element.lastName + " " + element.parentName +
                                "</td><td>" + data.dictionary.audiences[element.audience] +
                                "</td><td>" + data.dictionary.profiles[element.profile] +
                                "</td><td>" + "<div Class =" + isBel(element.needBel) + "/>" + "</td></tr>";

                },
                optionTemplate: function (value, text) {
                        return "<option value=" + value + ">" + text + "</option>"
                }
        }

        selectManager = {
                corps: "",
                place: "",
                audience: "",
                input: "",
                setCorps: function (value) {
                        this.corps = value;
                        $("#emptyOption").remove();
                        initPlacesSelect();
                },
                setPlace: function (value = "") {
                        this.place = value;
                        callbackFunction(value, initAudienceSelect)
                },
                setAudience: function (value = "") {
                        this.audience = value;
                        data.currentPupils = data.getPupils(this.getSelectionMode())
                        fillTablePupils(data.currentPupils);
                },
                setInput: function (value = "") {
                        if (value != "") {
                                let pupils = data.currentPupils.filter(p => p.firstName.includes(value));
                                fillTablePupils(pupils);
                        }
                        else{
                                fillTablePupils(data.currentPupils);
                        }
                },
                resetInput: function () {
                        fillTablePupils(data.currentPupils);
                },
                getSelectionMode: function () {
                        if (this.audience != "") return { key: "audience", value: this.audience };
                        if (this.place != "") return { key: "place", value: this.place };
                        if (this.corps != "") return { key: "corps", value: this.corps };
                }
        }

        sortingDirection = -1;
        dragElement = {};

        corpsProm = requestGenerator('https://lyceumexams.herokuapp.com/api/corpses', "corpses");
        pupilsProms = requestGenerator('https://lyceumexams.herokuapp.com/api/pupils', "pupils");
        dictProm = requestGenerator('https://lyceumexams.herokuapp.com/api/dictionary', "dictionary");

        Promise.all([corpsProm, pupilsProms, dictProm]).then(initCorpsSelect); 

        $(document).on("change", "select.btn", selectChange);
        $(document).on("keyup", "input.btn", selectChange)
        $(document).on("search", "input.btn", () => selectManager.resetInput())
        $(document).on("click", ".sortable", sortTable);
        $(document).on("mousedown", ".draggable", mouseDownHandler);
        $(document).on("mousemove", 'body', mouseMoveHandler);
        $(document).on("mouseup", 'body', mouseUpHandler)

        function selectChange(e) {
                let value, setterName;
                element = e.target;
                element.tagName
                value = element.tagName == "SELECT" ? element.options[element.selectedIndex].value : element.value;
                elementdata = element.getAttribute('data-type').charAt(0).toUpperCase() + element.getAttribute('data-type').slice(1);
                setterName = 'set' + elementdata;
                selectManager[setterName.toString()](value);
        }

        function sortTable(e) {
                let rows, dictionaryItem;
                elementdata = e.target.getAttribute('data-type');
                rows = data.currentPupils;
                sortingDirection *= -1;
                if (elementdata == 'firstName' || elementdata == 'needBel') {
                        rows.sort(sortTableWithoutDictionaryHelp);
                }
                else {
                        dictionaryItem = returnDictionaryItem();
                        rows.sort(sortTableWithDictionaryHelp);
                }
                fillTablePupils(rows);

                sortingDirection > 0 ? $(e.target).append(ArrowUp) : $(e.target).append(ArrowDown);

                function sortTableWithoutDictionaryHelp(a, b) {
                        if (a[elementdata] < b[elementdata]) {
                                return -1 * sortingDirection;
                        }
                        if (a[elementdata] > b[elementdata]) {
                                return 1 * sortingDirection;
                        }
                        return 0;
                }

                function sortTableWithDictionaryHelp(a, b) {
                        if (dictionaryItem[a[elementdata]] < dictionaryItem[b[elementdata]]) {
                                return -1 * sortingDirection;
                        }
                        if (dictionaryItem[a[elementdata]] > dictionaryItem[b[elementdata]]) {
                                return 1 * sortingDirection;
                        }
                        return 0;
                }

                function returnDictionaryItem() {
                        if (elementdata == 'audience') return data.dictionary.audiences;
                        if (elementdata == 'profile') return data.dictionary.profiles;
                }
        }

        function mouseDownHandler(e) {
                if (e.which != 1) {
                        return
                }
                dragElement.element = e.target.closest('.draggable')
                if (dragElement.element) {
                        dragElement.x = e.pageX;
                        dragElement.y = e.pageY;
                }
        }

        function mouseMoveHandler(e) {
                let moveX, moveY;
                if (!dragElement.element) return;

                moveX = e.pageX - dragElement.x;
                moveY = e.pageY - dragElement.y;
                if (Math.abs(moveX) < 10 && Math.abs(moveY) < 10) return;
                if (!dragElement.avatar) dragElement.avatar = dragElement.element.cloneNode(true);
                startDrag();
                dragElement.avatar.style.left = e.pageX + 10 + 'px';
                dragElement.avatar.style.top = e.pageY + 10 + 'px';
        }

        function mouseUpHandler(e) {
                if (dragElement.avatar) {
                        finishDrag(e);
                }
                dragElement = {};
        }

        function finishDrag(e) {
                dropElement = findDroppable(e);
                if (dropElement) {
                        replaceAudiences();
                        document.body.removeChild(dragElement.avatar);
                }
                else document.body.removeChild(dragElement.avatar);

        }

        function replaceAudiences() {
                let pupil, oldAudience, newAudince;
                pupil = data.getPupilById(dragElement.element.id);
                oldAudience = data.getAudienceByIdInCurrentCorps(pupil.audience);
                newAudince = data.getAudienceByIdInCurrentCorps(dropElement.id);
                if (pupil.needBel != newAudince.bel) {
                        alert('Error'); return;
                }
                pupil.audience = newAudince._id;
                newAudince.count++;
                oldAudience.count--;
                callbackFunction(selectManager.place, fillTableAudiences);
                fillTablePupils(data.currentPupils);
        }

        function findDroppable(e) {
                dragElement.avatar.hidden = true;
                element = document.elementFromPoint(e.clientX, e.clientY);
                dragElement.avatar.hidden = false;
                if (element == null) return null;
                return element.closest('.droppable');
        }

        function startDrag() {
                let avatar = dragElement.avatar;
                document.body.appendChild(avatar);
                avatar.style.zIndex = 1000;
                avatar.style.position = 'absolute';
        }

        function initCorpsSelect() {
                html = EmptyOption;
                data.corpses.forEach(c => { html += templates.optionTemplate(c.alias, c.name) })
                $("#corpses").html(html);
        }

        function initPlacesSelect() {
                html = DefaultOption;
                data.getCorps().places.forEach(c => { html += templates.optionTemplate(c._id, c.code) })
                $("#places").html(html).trigger('change');
        }

        function initAudienceSelect(places) {
                html = DefaultOption;
                for (p in places) {
                        places[p].audience.forEach(a => { html += templates.optionTemplate(a._id, a.name) });
                }
                $("#audiences").html(html).trigger('change');
                fillTableAudiences(places);
        }

        function fillTableAudiences(places) {
                html = "";
                $("#audiencesTable tbody > tr").remove();
                $.each(places, getAudienceTableHtml);
                $("#audiencesTable tbody").html(html);

                function getAudienceTableHtml(index) {
                        places[index].audience.forEach(element => { html += templates.audiencesTableTemplate(element) });
                }
        }

        function fillTablePupils(pupils) {
                html = "";
                $("#pupilsTable tbody > tr").remove();
                $.each(pupils, (index) => { html += templates.pupilsTableTemplate(pupils[index], index) });
                $("#pupilsTable tbody").html(html);
                $('th>i').remove();
        }

        function requestGenerator(url, objectName) {
                return $.get(url).then(requestSuccessful);
                function requestSuccessful(result) {
                        data[objectName] = result;
                }
        }

        function callbackFunction(value, callback) {
                if (value != "") {
                        callback(data.getCorps().places.filter(p => { return p._id == value }));
                }
                else callback(data.getCorps().places);
        }

        function isBel(bel) {
                return bel ? "needBel" : "";
        }

})(jQuery)